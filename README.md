# cljscript

cljscript allows for standalone clojure-scripts with inline dependencies, using [tools-deps](https://clojure.org/guides/getting_started#_clojure_installer_and_cli_tools).

`Usage: cljscript some.clj`
Or better: put this script in the hashbang of some.clj: `#!/usr/bin/env /path/to/cljscript`.

*canonical repository: https://gitlab.com/eval/cljscript*  
[![discuss at Clojurians-Zulip](https://img.shields.io/badge/clojurians%20zulip-clojure-brightgreen.svg)](https://clojurians.zulipchat.com/#narrow/stream/151168-clojure)

## SAMPLE

Given executable file, `hello.clj`:
```clojure
#!/usr/bin/env /path/to/cljscript
"DEPS='clj-time=RELEASE'"

(require '[clj-time.core :as t])

(defn -main [& args]
  (if-let [[name] args]
    (println (str "Hello " name ", time is now " (t/now)))
    (println "Usage: hello.clj name")))
```

Then:
```shell
$ ./path/to/hello.clj
Usage: hello.clj name
$ ./path/to/hello.clj World
Hello World, time is now 2019-01-28T14:13:42.203Z
```

For INSTALL details, see below.

The header of your script can contain the following variables:
- MAIN_NS
- DEPS
- DEPS_FILE

## MAIN_NS

By default when your script contains a function '-main', it will be called with any commandline arguments:
```clojure
#!/usr/bin/env path/to/cljscript

(defn -main [& args]
  (prn :args args))
```

```shell
$ ./some.clj '1 and 2' 3 
("1 and 2" "3")
```

If (for whatever reason) you add a namespace, you can point to it's '-main' using MAIN_NS:
```clojure
#!/usr/bin/env path/to/cljscript
"MAIN_NS=hello"

(ns hello)
(defn -main [& args]
  (prn :args args))
```

## DEPS

Put any dependencies in the header of the cljscript, like so:
```clojure
#!/usr/bin/env /path/to/cljscript
"DEPS='clj-time=0.15.1;some-other=RELEASE'"
```

Also supported: git-repositories or local paths:
```clojure
"DEPS='https://github.com/user/my-repos.git=80cabf798866552153dbf10e31f405c58d06b808'"
"DEPS='foo=../some/path/relative/to/the/script-folder'"
```

Mind the following:
- like in the example above, always put single quotes around the value of DEPS
- as supported by clj, use 'RELEASE' for the latest version. While handy for development, you'd typically want to 'pin' your script to use specific versions.
- you can comment out the line if needed like any clojure line: ';;"DEPS=.."'

You can also point to a deps.edn file (relative to your script):
"DEPS_FILE=../deps.edn"

`DEPS_FILE` and `DEPS` are mutually exclusive, with `DEPS_FILE` taking precedence.

Note:
In order to limit any interference, cljscript is set to ignore '~/.clojure/deps.edn' (flag -Srepro). 
There's currently no flag in clj though to ignore $PWD/deps.edn.


## PROJECT INSTALL (recommended)

```shell
$ mkdir -p $PROJECT_ROOT/bin
$ curl https://gitlab.com/eval/cljscript/raw/master/cljscript > $PROJECT_ROOT/bin/cljscript-local
```

Then use the following hashbang:
```
#!/usr/bin/env bin/cljscript-local
```

Extra benefit: you can make customizations to the script (e.g. use aliases from you project's deps.edn or introduce other variables besides DEPS, MAIN_NS etc.).

Limitation: scripts can only be executed when `$PWD` is `$PROJECT_ROOT`.

Recommended: something like [direnv](https://direnv.net/#man/direnv-stdlib.1) can help you add the expanded value of `$PROJECT_ROOT/bin` to your $PATH **when 'within' the project**.
Put the following in `$PROJECT_ROOT/.envrc`:
```shell
PATH_add bin
```

Then with the following hashbang, scripts can be run from anywhere 'under' $PROJECT_ROOT:
```shell
#!/usr/bin/env cljscript-local
```


## GLOBAL INSTALL

```shell
# given ~/bin exists and is on the PATH
$ curl https://gitlab.com/eval/cljscript/raw/master/cljscript > ~/bin/cljscript
```

Then use the following hashbang:
```
#!/usr/bin/env cljscript
```

Downsides:
- you cannot share scripts unless others have (the exact same version of) cljscript on their PATH.
- upgrading cljscript might break existing scripts


`AUTHOR`: Gert Goet (git(hub|lab).com/eval)  
`LICENSE`: MIT license (see header of file)  
`INSPIRATION`: borkdude's https://gist.github.com/borkdude/8ad63f256e6f834f812b86a2c0e36415  

